all:
	dune build @all

clean:
	dune clean

docs:
	dune build @doc

install:
	dune install --prefix "/usr" -p css_merge

deps:
	opam install . --deps-only

.PHONY: all clean
