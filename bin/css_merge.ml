open StdLabels

module Args = struct
  type t =
    { out: string
    }

  let default =
    { out = ""
    }
end

let () =

  let out = ref ""
  and rest = ref [] in
  let speclist =
    [ ("--out", Arg.Set_string out, "Output file")
    ; ("-o", Arg.Set_string out, "Output file") ] in
  Arg.parse speclist (fun arg -> rest := arg::(!rest)) "css_merge";
  let rest = List.rev (!rest) in
  let css = List.fold_left rest
      ~init:Css_lib.Merge.empty
      ~f:(fun map arg ->
          let content = Stdio.In_channel.read_all arg in
          let css = Css.Parser.parse_stylesheet content in
          Css_lib.Merge.add_css map css
        ) in

  Stdio.Out_channel.with_file !out ~f:(fun channel ->
      let format = Format.formatter_of_out_channel channel in
      Css_lib.Print.css
        Css_lib.Print.minify_printer
        format
        (Css_lib.Merge.extract_css css)
    )
