=======
CSS_lib
=======

.. default-role:: literal

.. role:: ocaml(code)
   :language: ocaml

`css_lib` is a library for OCaml which allow to work with CSS file. The library
is build on top on `css-parser`_.

.. _css-parser: https://github.com/astrada/ocaml-css-parser/


Installation
============

The repository is hosted at this location :
http://git.chimrod.com/css_lib.git/. You can install the library with opam :

.. code-block:: bash

  opam pin add https://git.chimrod.com/css_lib.git

css_merge
=========

`css_merge` is a standalone program which combine multiple css document into a
single one. It produce a minified output resulting of all the document.

Usage
-----

.. code-block:: bash

 css_merge --out ${out_file} file1.css file2.css …

The file `file1.css` and `file2.css` will be combined together, the rule from
`file2.css` overrides the one given in `file1.css`.

Example
-------

.. code-block:: css

  body, p {
      color: "black";
  }

  body, a {
      color: "red";

  }

will become

.. code-block:: css

  p{color:"black"}a,body{color:"red"}
